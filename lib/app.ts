import * as express from 'express';
import * as cors from 'cors';
import * as bodyParser from 'body-parser';
import * as mongoose from 'mongoose';
import {ServiceRoutes} from './serviceRoutes';

class App {

    public app: express.Application;
    public router: ServiceRoutes = new ServiceRoutes();
    public mongoUrl: string = 'mongodb://127.0.0.1/key-center';

    constructor() {
        this.app = express();
        this.app.use(cors());
        this.config();
        this.router.routes(this.app);
        this.mongoSetup();
    }

    private config(): void {
        // Support application/json type post data
        this.app.use(bodyParser.json());
    }

    private mongoSetup(): void {
        mongoose.Promise = global.Promise;
        mongoose.connect(this.mongoUrl, {
            useNewUrlParser: true
        });
    }
}

export default new App().app;