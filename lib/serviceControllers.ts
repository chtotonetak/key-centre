import * as mongoose from 'mongoose';
import {Request, Response} from 'express';
import {UserKeySchema, ValidatorKeySchema} from './serviceModels';

const UserKey = mongoose.model('UserKey', UserKeySchema);
const ValidatorKey = mongoose.model('ValidatorKey', ValidatorKeySchema);

export class ServiceController {
    public addUserKey(req: Request, resp: Response) {
        UserKey.findOneAndUpdate({
            userID: req.params.userID,
        }, {
            publicKey: req.body.publicKey
        }, {
            upsert: true,
            new: true
        }, (err, userKey) => {
            if (err) {
                resp.send(err);
            }
            resp.json({
                status: true,
                keyID: userKey._id
            });
        });
    }

    public getUserKey(req: Request, resp: Response) {
        UserKey.findOne({
            userID: req.params.userID
        }, 'publicKey', (err, userKey) => {
            if (err) {
                resp.send(err);
            }
            resp.json(userKey);
        });
    }

    public addValidatorKey(req: Request, resp: Response) {
        ValidatorKey.findOneAndUpdate({
            validatorID: req.params.validatorID,
        }, {
            publicKey: req.body.publicKey
        }, {
            upsert: true,
            new: true
        }, (err, validatorKey) => {
            if (err) {
                resp.send(err);
            }
            resp.json({
                status: true,
                keyID: validatorKey._id
            });
        });
    }

    public getValidatorKey(req: Request, resp: Response) {
        ValidatorKey.findOne({
            validatorID: req.params.validatorID
        }, 'publicKey', (err, userKey) => {
            if (err) {
                resp.send(err);
            }
            resp.json(userKey);
        });
    }
}