import {Request, Response} from 'express';
import {ServiceController} from './serviceControllers';

export class ServiceRoutes {
    public serviceController: ServiceController = new ServiceController();

    public routes(app): void {
        // Welcome
        app.route('/')
            .get((req: Request, resp: Response) => {
                resp.status(200).send({
                    message: 'Welcome to the Key Certification Centre'
                });
            });

        // Get a validator's key
        app.route('/validator/:validatorID/key')
            .get(this.serviceController.getValidatorKey);
        // Add a validator's key
        app.route('/validator/:validatorID/key')
            .put(this.serviceController.addValidatorKey);

        // Get an user's key
        app.route('/user/:userID/key')
            .get(this.serviceController.getUserKey);
        // Add an user's key
        app.route('/user/:userID/key')
            .put(this.serviceController.addUserKey);
    }
}