import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

export const UserKeySchema = new Schema({
    userID: {
        type: String,
        required: "User ID is required"
    },
    publicKey: {
        type: String,
        required: "Public key value is required"
    }
});

export const ValidatorKeySchema = new Schema({
    validatorID: {
        type: String,
        required: "Validator ID is required"
    },
    publicKey: {
        type: String,
        required: "Public key value is required"
    }
});